name := "kvstore"

version := "0.1"

scalaVersion := "2.12.2"

libraryDependencies += "com.typesafe.akka" %% "akka-http" % "10.0.5"

mainClass in Compile := Some("kvstore.KVMaster")
