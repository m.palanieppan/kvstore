package kvstore

import java.security.MessageDigest
import java.util
import scala.collection.JavaConverters._

/**
  * Created by pala on 5/18/17.
  */
class ConsistentHashRing {

  val ringMap = new util.TreeMap[String, Int]()

  def serialize: String = {
    ringMap.asScala.aggregate("")({ (acc, kv) => acc + kv._1 + " " + kv._2 + "\n" }, { (kv1, kv2) => kv1 + "\n" + kv2 })
  }

  /**
    * Used during deserialization, to populate prehashed keys.
    * @param key
    * @param value
    * @return
    */
  def putHashedKey(key:String, value:Int) = {
    ringMap.put(key, value)
  }

  def put(rawKey:String, value:Int): Unit = {
    ringMap.put(ConsistentHashRing.hash(rawKey), value)
  }

  def remove(rawKey:String): Unit = {
    ringMap.remove(rawKey)
  }

  /***
    * Gets owner of a given key in the ring.
    * @param rawKey
    * @return
    */
  def getOwner(rawKey:String): Int = {
    val keyHash = ConsistentHashRing.hash(rawKey)
    var entry = ringMap.floorEntry(keyHash)
    if (entry == null) {
      entry = ringMap.ceilingEntry(keyHash)
    }

    entry.getValue()
  }

  def size(): Int = ringMap.size()
}

object ConsistentHashRing {
  def deserialize(data:String):ConsistentHashRing = {
    val ring = new ConsistentHashRing()
    data.split("\n").map(kvPair => {val vals = kvPair.split(" "); (vals(0), vals(1))})
      .foreach(p => ring.putHashedKey(p._1, p._2.toInt))

    return ring
  }

  def hash(key:String):String = {
    MessageDigest.getInstance("MD5").digest(key.getBytes()).map("%02X".format(_)).mkString
  }
}
