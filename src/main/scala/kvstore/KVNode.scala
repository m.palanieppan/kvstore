// start with a simple single process key value store, that listens on a port
package kvstore

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.stream.ActorMaterializer
import akka.http.scaladsl.server.Directives._

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}
import scala.io.StdIn

object KVNode{

  def main(args: Array[String]): Unit = {

    if (args.length == 0) {
      throw new RuntimeException("Port is required")
    }

    implicit val system = ActorSystem()
    implicit val materializer = ActorMaterializer()
    implicit val executionContext = system.dispatcher

    val route =
      put {
        pathPrefix("putKey" / Segment) { key =>
          entity(as[String]) { value =>
            KVStore.put(key, value)
            complete("Value saved")
          }
        }
      } ~
      get {
        pathPrefix("getKey" / Segment) { key =>
          KVStore.get(key) match {
            case Some(actualVal) => complete(actualVal)
            case None => complete(StatusCodes.NotFound)
          }
        }
      } ~
      get {
        // simple get call for lease check from master
        path("heartbeat") {
          complete("Ok")
        }
      }


    val port = args(0).toInt
    val bindingFuture = Http().bindAndHandle(route, "localhost", port)

    // talk to kv master and register yourself.
    val masterPort = Consts.MasterPortNumber
    val responseFuture: Future[HttpResponse] = Http().singleRequest(HttpRequest(
      uri = s"http://localhost:$masterPort/addNode", method = HttpMethods.POST, entity = HttpEntity(port.toString)))
    Await.result(responseFuture, Duration(1L, "s"))


    println(s"Server online at http://localhost:$port/\nPress RETURN to stop...")
    StdIn.readLine() // let it run until user presses return
    bindingFuture
      .flatMap(_.unbind()) // trigger unbinding from the port
      .onComplete(_ => system.terminate()) // and shutdown when done

  }
}
