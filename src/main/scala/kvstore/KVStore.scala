package kvstore

import scala.collection.mutable

/**
  * Created by pala on 4/26/17.
  */
object KVStore {
  val store = mutable.Map[String, String]()
  def put(key:String, value:String): Unit = {
    store(key) = value
  }

  def get(key:String) = {
    store.get(key)
  }
}
